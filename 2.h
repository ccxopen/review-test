//
//  BannerCollectionHeaderView.h
//  it-ww-ios-emag
//
//  Created by MartinLi on 14-10-14.
//  Copyright (c) 2014年 NMG. All rights reserved.
//

#define LeftBannerWidth         ([UIDevice isIpad] ? 515.0 : MIN_WIDTH)
#define LeftBannerHeight        ([UIDevice isIpad] ? 300.0 : MIN_WIDTH*300.0/515.0)

//網絡鏈接iphone和ipad比例
#define kWebSCALE ([UIDevice isIpad]?1.0:(320/768.0))

//网页链接按钮的高度
#define kCGWebViewButtonHeight ([UIDevice isIpad] ? 60*kWebSCALE : 60*kWebSCALE+10)

@class BannerCollectionHeaderView;
@protocol BannerCollectionHeaderViewDelegate <NSObject>
@optional

-(void)bannerCollectionHeaderView:(BannerCollectionHeaderView *)bannerCollectionView buttonClickTag:(int)index;

@end
#import <UIKit/UIKit.h>
#import "NIPagingScrollView.h"

@interface BannerCollectionHeaderView : UICollectionReusableView

@property (nonatomic ,strong)  UIView *rightBanner;
@property (nonatomic,weak) id<BannerCollectionHeaderViewDelegate> delegate;

@end
